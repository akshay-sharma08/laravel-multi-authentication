<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::match(['GET', 'POST'], "login", [AuthController::class, "login"])->name("login")->middleware("guest");
Route::get("/logout", [AuthController::class, "logout"]);

// ? ***************************************** Wth the web auth only ***************************************** */
Route::middleware(['auth'])->group(function () {
    Route::get('/', function () {
        return view('welcome');
    });
    Route::get('/profile', function () {
        return view('profile');
    });
});

// ? ***************************************** Wth the company auth only ***************************************** */
Route::middleware(['auth:company'])->group(function () {
    Route::get('/company', function () {
        return view('company.index');
    });
});

// ? ***************************************** Wth the both web and company auth only ***************************************** */
Route::middleware(["auth:web,company"])->group(function () {
    Route::get("/create-user", function () {
        return view("company.new-user");
    });
});
