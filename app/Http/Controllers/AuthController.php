<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        if ($request->method() == "GET") {
            return view("login");
        }

        if (Auth::guard("web")->attempt(["email" => $request->email, "password" => $request->password])) {
            return redirect("/");
        }
        if (Auth::guard("company")->attempt(["email" => $request->email, "password" => $request->password])) {
            return redirect("/company");
        }

        dd("error");
    }

    public function logout()
    {
        // dd("here");
        if (Auth::guard("web")->check()) {
            Auth::guard("web")->logout();
        }

        if (Auth::guard("company")->check()) {
            Auth::guard("company")->logout();
        }

        return redirect("login");
    }
}
